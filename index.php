<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div id="data">
</div>
<div>
    <form>
        Name: <input type="text" id="name"><br/>
        Class: <input type="text" id="class"><br/>
        <input type="button" id="add" value="Add">
    </form>
</div>
<script>
    $( document ).ready(function() {
        reload();
        $("#add").click(function () {
            var name = $("#name").val();
            var myclass = $("#class").val();
            $.post( "addNew.php", { name: name, class: myclass })
                .done(function( data ) {
                    reload()
                });
        })
    });
     function deleteItem(id) {
        var url = "delete.php?id="+id;
        $.ajax({url: url, success: function(result){
                reload();
        }});
    }
    function reload() {
        var url = "list.php"
        $.ajax({url: url, success: function(result){
                $("#data").html(result)
            }});
    }
</script>
